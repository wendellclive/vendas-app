import Link from "next/link";

export const Menu: React.FC = () => {
  return (
    <aside className="menu column is-3 is-narrow-mobile is-fullheight section is-hidden-mobile">
      <p className="menu-label is-hidden-touch">Minhas Vendas</p>
      <ul className="menu-list">
        <MenuItem href="/" label="Home" />
        <MenuItem href="/cadastros/produtos" label="Cadastros Produtos" />
        <MenuItem href="/" label="Relatorios" />
      </ul>
      {/* <p className="menu-label is-hidden-touch">Administração</p>
      <ul className="menu-list">
        <li>
          <a className="is-active">Manage Your Team</a>
          <ul>
            <MenuItem href="/" label="Members" />
            <MenuItem href="/" label="Plugins" />
            <MenuItem href="/" label="Add a member" />
          </ul>
        </li>
      </ul>
      <p className="menu-label is-hidden-touch">Usuarios</p>
      <ul className="menu-list">
        <MenuItem href="/" label="Cadastro" />
        <MenuItem href="/" label="Relatorio" />
      </ul> */}
      <p className="menu-label is-hidden-touch">Sair</p>
    </aside>
  );
};

interface MenuItemProps {
  href: string;
  label: string;
}

const MenuItem: React.FC<MenuItemProps> = (props: MenuItemProps) => {
  return (
    <li>
      <Link href={props.href}>
        <span className="icon"></span> {props.label}
      </Link>
    </li>
  );
};
