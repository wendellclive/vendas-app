"use client";

import { useState } from "react";
import { Layout, Input } from "@/components";

export const CadastroProdutos: React.FC = () => {
  const [sku, setSku] = useState<string>("");
  const [preco, setPreco] = useState<string>("");
  const [nome, setNome] = useState<string>("");
  const [descricao, setDescricao] = useState<string>("");

  const submit = () => {
    const produto = {
      sku,
      preco: preco,
      nome,
      descricao,
    };
    console.log(produto);
  };

  return (
    <Layout titulo="Cadastro de Produtos">
      <div className="columns">
        <Input
          id="inputSku"
          label="SKU: *"
          columnClasses="is-half"
          onChange={setSku}
          placeholder="Digite o SKU do produto"
        />
        <Input
          id="inputSkuPreco"
          label="Preço: *"
          columnClasses="is-half"
          onChange={setPreco}
          placeholder="Digite o preço do produto"
        />
      </div>
      <div className="columns">
      <Input
          id="inputNome"
          label="Nome do Produto: *"
          columnClasses="is-full"
          onChange={setNome}
          placeholder="Digite o nome do produto"
        />
      </div>
      <div className="columns">
        <div className="field is-full column">
          <label className="label" htmlFor="inputDescricao">
            Descrição do Produto: *
          </label>
          <div className="control">
            <textarea
              className="textarea"
              id="inputDescricao"
              value={descricao}
              onChange={(e) => setDescricao(e.target.value)}
              placeholder="Digite a Descrição do produto"
            />
          </div>
        </div>
      </div>
      <div className="field is-grouped">
        <div className="control">
          <button className="button is-link" onClick={submit}>
            Salvar
          </button>
        </div>
        <div className="control">
          <button className="button">Voltar</button>
        </div>
      </div>
    </Layout>
  );
};
